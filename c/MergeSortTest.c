#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <sys/time.h>
#include "p_merge_sort.h"


int main () {
    // Initialized data for the array.
    long i;
    
    // Allocating the resources.
    long array_len = 3000000;
    int *single_array = malloc(array_len * sizeof(int));
    int *parallel_array = malloc(array_len * sizeof(int));

    // Initialized data for the time.
    struct timeval start, end;
    double time_diff;

    // Generating random values for single_array and making a copy of it.
    for(i = 0; i < array_len; i++){
        single_array[i] = rand() % 100;
        parallel_array[i] = single_array[i];
    }

    // Single Threaded Merge Sort
    gettimeofday(&start, NULL);
    merge_sort(single_array, array_len);
    gettimeofday(&end, NULL);

    time_diff = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec))/1000000.0;

    printf("\nSingle threaded merge sort Time: %lf seconds\n", time_diff);
    printf("Single threaded merge sort correct: %s\n", (is_sorted(single_array, array_len) == 1) ? "True" : "False");
    
    // Multi Threaded Merge Sort
    gettimeofday(&start, NULL);
    parallel_merge_sort(parallel_array, array_len, omp_get_num_procs());
    gettimeofday(&end, NULL);

    time_diff = ((end.tv_sec * 1000000 + end.tv_usec) - (start.tv_sec * 1000000 + start.tv_usec))/1000000.0;

    printf("\nMulti threaded merge sort Time: %lf seconds\n", time_diff);
    printf("Multi threaded merge sort correct: %s\n", (is_sorted(parallel_array, array_len) == 1) ? "True" : "False");

    printf("\nSame Results: %s\n", (verify_arrays(single_array, parallel_array) == 1) ? "True" : "False");

    // Freeing the resources.
    free(single_array);
    free(parallel_array);

    return 0;
}
