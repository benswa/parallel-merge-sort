#include <stdio.h>
#include <stdlib.h>

int main () {
	long i;
	long array_len = 200;
	int *array = malloc(array_len * sizeof(int));

	for(i = 0; i < array_len; i++){
		array[i] = i;
		printf("%d\n", array[i]);
	}

	free(array);

	return 0;
}