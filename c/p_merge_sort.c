#include <stdlib.h>
#include <omp.h>
#include <stdio.h>

int is_sorted(int *array, long array_len){
    // Loops though the entire array to check if the
    // array is sorted. We check from index 0 to
    // array_len - 1 because we are comparing the current
    // value at current index with value at index + 1.
    //
    // Returns 1 if sorted or 0 if not sorted.
    long i;
    for(i = 0; i < (array_len - 1); i++){
        if(array[i] > array[i+1]){
            return 0;
        }
    }
    return 1;
}

int verify_arrays(int *array1, int *array2){
    // Returns 0 if not equal otherwise returns a 1.
    int i;
    int array1_len = sizeof(array1) / sizeof(array1[0]);
    int array2_len = sizeof(array2) / sizeof(array2[0]);
    if(array1_len != array2_len){
        return 0;
    }
    for(i = 0; i < array1_len; i++){
        if(array1[i] != array2[i]){
            return 0;
        }
    }
    return 1;
}


void merge(int *array, long array_len, long index_split){
    // This function does merging of two sub-arrays.
    // The merging operation will modify the contents of
    // the original array passed in.
    long i;
    int *merged_array = malloc(array_len * sizeof(int));

    long right_length = array_len - index_split;
    long right_index = index_split;
    long right_index_end = right_index + right_length;

    long left_length = index_split;
    long left_index = 0;
    long left_index_end = left_index + left_length;

    for(i = 0; i < array_len; i++){
        if((right_index < right_index_end) && (left_index < left_index_end)){
            // If both the sub-arrays have elements to merge
            if(array[left_index] <= array[right_index]){
                merged_array[i] = array[left_index];
                left_index++;
            }
            else{
                merged_array[i] = array[right_index];
                right_index++;
            }
            continue;
        }
        if((right_index == right_index_end) && (left_index < left_index_end)){
            // If left sub-array still have some elements unmerged
            merged_array[i] = array[left_index];
            left_index++;
            continue;

        }
        if((right_index < right_index_end) && (left_index == left_index_end)){
            // If right sub-array still have some elements unmerged
            merged_array[i] = array[right_index];
            right_index++;
            continue;
        }
    }

    // Writing the merge_array contents back into the original array
    for(i = 0; i < array_len; i++){
        array[i] = merged_array[i];
    }

    free(merged_array);
}

void merge_sort(int *array, int array_len){
    if(array_len < 2){
        return;
    }
    int index_split = array_len / 2;
    
    // Recurse on Left Half
    merge_sort(array, index_split);
    
    // Recurse on Right Half
    merge_sort(array + index_split, array_len - index_split);
    
    // Merge the result
    merge(array, array_len, index_split);
}

void parallel_merge_sort(int *array, int array_len, int core_count){
    // Used guiding examples.
    // http://www1.chapman.edu/~radenski/research/papers/mergesort-pdpta11.pdf
    // Current implementation using omp parallel sections does not use more
    // than two cores. The very first 'sectioning' already assigned the work to
    // a specific core and apparently, it doesn't seem to allow more division of
    // work. The best alternative to this approach is to use pthreads instead.

    if(core_count == 1){
        // If the work cannot be divided further between the cores, start running
        // the regular merge_sort algorithm and begin the sorting process.
        merge_sort(array, array_len);
    }
    if(core_count > 1){
        // If there are cores still avaliable, keep dividing the workload.
        int index_split = array_len / 2;
        int avaliable_cores = core_count / 2;
        #pragma omp parallel sections
        {
            #pragma omp section
            // Recurse on Left Half
            parallel_merge_sort(array, index_split, avaliable_cores);
            
            #pragma omp section
            // Recurse on Right Half
            parallel_merge_sort(array + index_split, array_len - index_split, avaliable_cores);
        }
        // Merge the result
        merge(array, array_len, index_split);
    }
}
