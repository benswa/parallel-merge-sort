#ifndef P_MERGE_SORT_H_
#define P_MERGE_SORT_H_

// Checks if the array is sorted.
int is_sorted(int *array, long array_len);

// Any array passed in will be modified. Single Thread Version.
void merge_sort(int *array, long array_len);

// Any array passed in will be modified. Multi Thread Version.
void parallel_merge_sort(int *array, int array_len, int core_count);

// Verify that two arrays are the same.
int verify_arrays(int *array1, int *array2);

#endif
