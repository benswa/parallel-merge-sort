{- Benson Liu - 1236292

   Parallelized merge sort with single threaded and multi-threaded.

   The merge sorts returns a sorted version of the input_list.
   The demo does not show the lists, but show only the time
   taken to do the sorting for single threaded and multi-threaded.
-}

-- Importing libraries
import System.Random
import Data.List
import GHC.Conc (numCapabilities) -- From: http://stackoverflow.com/questions/8041813/how-to-find-the-number-of-cores-at-runtime-in-haskell
import Control.Monad.Par (runPar, spawnP, get)
import Data.Time.Clock (diffUTCTime, getCurrentTime)
import Control.DeepSeq (deepseq)


random_list :: Int -> IO [Int]
random_list size = do
    seed <- getStdGen
    return $ take size $ randomRs (0, 100) seed


merge :: ([Int], [Int]) -> [Int]
merge ([], l2) = l2
merge (l1, []) = l1
merge (l1@(l1_head : l1_tail), l2@(l2_head : l2_tail))
            | l1_head <= l2_head = l1_head : (merge (l1_tail, l2))  --  If l1_head is less than l2_head
            | otherwise = l2_head : (merge (l1, l2_tail))           --  If l2_head is less than 12_head


-- From: http://stackoverflow.com/questions/19074520/how-to-split-a-list-into-two-in-haskell
split_half :: [Int] -> ([Int], [Int])
split_half list = splitAt (((length list) + 1) `div` 2) list


merge_sort :: [Int] -> [Int]
merge_sort [] = []
merge_sort [single_element] = [single_element]
merge_sort list = merge ((\(l1, l2) -> (merge_sort l1, merge_sort l2)) (split_half list))


p_merge_sort :: [Int] -> Int -> [Int]
p_merge_sort [] _ = []
p_merge_sort [single_element] _ = [single_element]
p_merge_sort list threads = runPar $ do
            let (l1, l2) = split_half list
            -- Current code can only use an even amount of cores, odd numbers will result in an 'unused core'
            sorted_l1 <- if ((div threads 2) > 1) then (spawnP (p_merge_sort l1 (div threads 2))) else (spawnP (merge_sort l1))
            sorted_l2 <- if ((div threads 2) > 1) then (spawnP (p_merge_sort l2 (div threads 2))) else (spawnP (merge_sort l2))
            sema_l1 <- get sorted_l1    -- Waits for sorted_l1 to finish
            sema_l2 <- get sorted_l2    -- Waits for sorted_l2 to finish
            return $ merge (sema_l1, sema_l2)


main = do
    single_thread_list <- random_list 300000
    let multi_thread_list = single_thread_list

    start_single <- getCurrentTime
    let single_sorted_list = merge_sort single_thread_list
    end_single <- deepseq single_sorted_list getCurrentTime   -- Forced Evaluation of sorted_list before running getCurrentTime
    putStrLn $ "Threads: 1  Single Threaded Time: " ++ (show (end_single `diffUTCTime` start_single))

    start_multi <- getCurrentTime
    --let multi_sorted_list = p_merge_sort multi_thread_list
    let multi_sorted_list = p_merge_sort multi_thread_list numCapabilities
    end_multi <- deepseq multi_sorted_list getCurrentTime   -- Forced Evaluation of sorted_list before running getCurrentTime
    putStrLn $ "Threads: " ++ (show numCapabilities) ++ "  Multi Threaded Time: " ++ (show (end_multi `diffUTCTime` start_multi))