"""
Parallelized merge sort using pool vs single core version.

The merge sorts returns a sorted version of the input_list.
The demo does not show the lists, but show only the time
taken to do the sorting for single threaded and multi-threaded.
"""


__author__ = 'Ben'


from multiprocessing import Pool
from time import time
from random import randint
try:
   from os import cpu_count  # For Python 3
except ImportError:
   from multiprocessing import cpu_count  # For Python 2


def find_core_count():
    """
    This function finds the number of processors available.
    """
    try:
        core_count = cpu_count()
    except NotImplementedError or None:
        core_count = 1
    return core_count


def merge(left_list, right_list):
    """
    This function returns a list that is merged and sorted.
    """
    merged_list = []
    left_list_index_tracker = right_list_index_tracker = 0
    while (left_list_index_tracker < len(left_list)) and (right_list_index_tracker < len(right_list)):
        # For as long as both the lists still have some unsorted element. Compare the elements in the
        # left and right array.
        if left_list[left_list_index_tracker] <= right_list[right_list_index_tracker]:
            merged_list.append(left_list[left_list_index_tracker])
            left_list_index_tracker += 1
        else:
            merged_list.append(right_list[right_list_index_tracker])
            right_list_index_tracker += 1
    
    if (left_list_index_tracker == len(left_list)) and (right_list_index_tracker < len(right_list)):
        # If the left_list is completely sorted and the right_list still have elements to sort.
        # Add whatever is left in the right_list to the back of the merged_list.
        merged_list.extend(right_list[right_list_index_tracker:])
    if (right_list_index_tracker == len(right_list)) and (left_list_index_tracker < len(left_list)):
        # If the right_list is completely sorted and the left_list still have elements to sort.
        # Add whatever is left in the left_list to the back of the merged_list.
        merged_list.extend(left_list[left_list_index_tracker:])
    return merged_list


def merge_sort(input_list):
    """
    Merge sort algorithm.

    This function does not modify input_list because it returns the
    result from merge that is a sorted copy of input_list.
    """
    if len(input_list) <= 1:
        # Base case, if the length of the list is 1, return the list.
        return input_list

    # Divide and Conquer
    index_split_point = int(len(input_list) / 2)
    return merge(merge_sort(input_list[:index_split_point]), merge_sort(input_list[index_split_point:]))


def partition_list(input_list, process_count):
    """
    This function is responsible for splitting the input_list into segments.

    Since not all numbers can be split evenly:
    Example Input List: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]

    We simply take what we can evenly split and partition the list.
    int(10 / 8) = 1
    [[1], [2], [3], [4], [5], [6], [7], [8]]

    We still need to handle the remaining numbers that could not be evenly
    divided up so we distribute those last few numbers.
    Ex: 10 % 8 = 2
    [[1, 9], [2, 10], [3], [4], [5], [6], [7], [8]]
    """
    input_list_length = len(input_list)
    sub_division_size = int(float(input_list_length) / process_count)
    modulo_remainder = input_list_length % process_count

    partitioned_list = list()

    # Splits the list into even parts, this part does not handle 'remainders'.
    for sub_list_index in range(0, process_count):
        sub_list = list()
        for offset in range(0, sub_division_size):
            sub_list.append(input_list[(sub_list_index * sub_division_size) + offset])
        partitioned_list.append(sub_list)

    # Takes the remaining numbers at the end of the input_list and proceeds to distributes them
    # across the sub-lists. It messes up the order of the original list by taking the
    # last few numbers of the input_list and distributes it across the sub-lists, if the above
    # loop did not evenly distribute every single number.
    remaining_numbers = 0
    for sub_list_index in range(0, process_count):
        if modulo_remainder is remaining_numbers:
            break
        partitioned_list[sub_list_index].append(input_list[(sub_division_size * process_count) + remaining_numbers])
        remaining_numbers += 1

    return partitioned_list


def merge_currying(two_lists):
    (first_list, second_list) = two_lists
    return merge(first_list, second_list)


def parallel_merge_sort(input_list, process_count):
    """
    This function is the parallel version of merge_sort and it does also call
    the function merge_sort.

    This function also does not modify input_list.
    """
    # Evenly divide the input_list indices to segment the input_list into sub-lists that equal the same
    # amount of processes to create
    partitioned_list = partition_list(input_list, process_count)

    # Pool of worker processes created
    pool = Pool(processes=process_count)
    sorted_sub_lists = pool.map(merge_sort, partitioned_list)

    # If we have more than one sorted sub-lists, combine them with merge.
    while len(sorted_sub_lists) > 1:
        # Merging the sorted sub-list in pairs
        paired_lists = list()
        
        for i in range(0, len(sorted_sub_lists), 2):
            # i skipping 1 value at a time so we can pair two sub_lists into a tuple
            paired_lists.append((sorted_sub_lists[i], sorted_sub_lists[i+1]))
        sorted_sub_lists = pool.map(merge_currying, paired_lists)

    return sorted_sub_lists[0]


if __name__ == '__main__':
    """
    Runs a test that demonstrates the performance of the sorting algorithms.
    """
    # Return number between 2*10^5 to 10^6, sizes smaller than 2*10^5 isn't worth parallel merge sorting on my CPU
    size_of_list = randint(2*10**5, 10**6)
    print('List length: ', size_of_list)

    # Generating a list of random numbers. Also Saving the original list for use on different sorting algorithms.
    original_list = list()
    loop_count = 0
    while loop_count < size_of_list:
        original_list.append(randint(0, 100))
        loop_count += 1

    # Single process merge sort a copy of the list.
    random_list = original_list[:]
    start = time()  # start time
    single_threaded_sorted_list = merge_sort(random_list)

    time_diff = time() - start  # stop time
    print('Single process merge sort time (sec): ', time_diff)

    # Multiprocess merge sort.
    random_list = original_list[:]
    start = time()
    number_of_processes = find_core_count()  # One process for one core
    multi_threaded_sorted_list = parallel_merge_sort(random_list, number_of_processes)

    time_diff = time() - start
    print("CPU Count: ", number_of_processes)
    print('Multiprocess merge sort time (sec): ', time_diff)